<?php

use App\Lib1 as Lib1;
use App\Lib2\MyClass as Obj;

require_once('lib1.php');
require_once('lib2.php');

header('Content-type: text/plain');

echo Lib1\MYCONST . "\n";
echo Lib1\MyFunction() . "\n";
echo Lib1\MyClass::WhoAmI(). "\n";
echo Obj::WhoAmI() . "\n";
