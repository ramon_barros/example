<?php

ini_set('display_errors', 'on');
error_reporting(E_ALL);

spl_autoload_register(function($class){
	require_once(str_replace('\\','/',$class . '.php'));
});

use Project\Model\User, Project\Model\Post as Article;

$user = new User('Ramon Barros');
$article = new Article();
$article->setAuthor($user)
        ->setTitle('Example article')
        ->setContent('Article contents');

echo PHP_EOL . '<br />';
printf("Author: %s", $article->getAuthor()->getName());
echo PHP_EOL . '<br />';

printf("Title: %s", $article->getTitle());
echo PHP_EOL . '<br />';

printf("Content: %s", $article->getContent());

echo "<pre>";
var_dump($user);
var_dump($article);
echo "</pre>";